lists.# Create a list
fruits = ["apple", "banana", "cherry", "water melon"]

# Access elements by index
print(fruits[0])  

# Modify elements
fruits[1] = "grape"
print(fruits) 

# Add elements
fruits.append("orange")
print(fruits)  

# Remove elements
fruits.remove("cherry")
print(fruits) 

# List length
length = len(fruits)
print(length) 

# Check if an element exists
print("banana" in fruits) 

# Sort the list
fruits.sort()
print(fruits) 
